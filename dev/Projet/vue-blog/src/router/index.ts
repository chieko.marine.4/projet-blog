import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ContactView from '@/views/ContactView.vue'
import Plat_dessertList from '@/views/Plat _dessertList.vue'
import Plat_dessertVue from '@/views/Plat_dessertVue.vue'
import ArticleListView from '@/views/ArticleListView.vue'
import ArticleOneView from '@/views/ArticleOneView.vue'



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', component: HomeView},
    { path: '/article', component: ArticleListView },
    { path: '/article/:id', component:  ArticleOneView},
    { path: '/contact', component: ContactView },
    { path: '/plat_dessert', component: Plat_dessertList },
    { path: '/plat_dessert/:id', component: Plat_dessertVue }
  ]
})

export default router
