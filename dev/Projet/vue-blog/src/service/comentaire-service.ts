import type { Commentaire } from "@/entities";
import axios from "axios";

export async function fetchCommentaires() {
    const reponse = await axios.get<Commentaire[]>('http://localhost:8080/api/comment');
    return reponse.data;
}

export async function fetchIdcategoCommentaires(id_categorie:any) {
    const reponse = await axios.get<Commentaire[]>('http://localhost:8080/api/comment/catego' + id_categorie);
    return reponse.data;
}

export async function postCommentaires(commentaire:Commentaire) {
    const reponse = await axios.post<Commentaire>('http://localhost:8080/api/comment/', commentaire);
    return reponse.data;
}

export async function deleteCommentaires(id:any) {
    await axios.delete<Commentaire>('http://localhost:8080/api/comment/' + id);
}

export async function updateCommentaires(commentaire:Commentaire) {
    const reponse = await axios.put<Commentaire>('http://localhost:8080/api/comment/'+commentaire.id, commentaire);
    return reponse.data;
}
