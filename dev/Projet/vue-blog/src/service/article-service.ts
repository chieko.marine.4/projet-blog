import ArticleList from "@/components/ArticleList.vue";
import type { Article } from "@/entities";
import axios from "axios";

export async function fetchAllArticles() {
    const reponse = await axios.get<Article[]>('http://localhost:8080/api/article')
    return reponse.data
}

export async function fetchOneArticles(id:any) {
    const reponse1 = await axios.get<Article>('http://localhost:8080/api/article/' +id)
    return reponse1.data
}

export async function fetchIdCategoArticles(id_categorie:any) {
    const reponse1 = await axios.get<Article[]>('http://localhost:8080/api/article/categorie/' + id_categorie)
    return reponse1.data
}


export async function postArticles(article:Article) {
    const reponse = await axios.post<Article>('http://localhost:8080/api/article/',article)
    return reponse.data
}

export async function deleteArticles(id:any) {
    await axios.delete<void>('http://localhost:8080/api/article/' + id)
}

export async function updateArticles(article:Article) {
    const reponse1 = await axios.put<Article>('http://localhost:8080/api/article/' + article.id, article)
    return reponse1.data
}

