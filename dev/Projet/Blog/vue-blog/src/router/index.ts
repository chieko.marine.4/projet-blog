import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ArticleListView from '@/views/ArticleListView.vue'
import ArticleOneView from '@/views/ArticleOneView.vue'
import CreateArticleView from '@/views/CreateArticleView.vue'
import ArticleCategorieView from '@/views/ArticleCategorieView.vue'
import RechercheBar from '@/components/RechercheBar.vue'
import LoginView from '@/components/LoginView.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', component: HomeView},
    { path: '/article', component: ArticleListView },
    { path: '/article/:id', component:  ArticleOneView},
    { path: '/article/categorie/:id', component:  ArticleCategorieView},
    { path: '/login', component:  LoginView},
    { path: '/test', component:  RechercheBar}
    
  ]
})

export default router
