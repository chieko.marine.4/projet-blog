export interface Article{
    id?: number,
    titre: string,
    photo: string,
    text: string,
    date_article?: string,
    vue?: number,
    id_categorie?: number,
}

export interface Commentaire{
    id: number,
    author: string,
    commentaire: string,
    date_commentaire: string,
    id_article: number
}

export interface Categorie{
    id: number,
    name: string
}

export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
}
