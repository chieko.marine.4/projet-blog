package co.simplon.projet.projetblog.controller;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest 
@AutoConfigureMockMvc 
@Sql("/database.sql")
public class CommentaireControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/comment"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['author']").exists())
        .andExpect(jsonPath("$[0]['author']").value("Mike"));
    }

    //? date isNotEmpty? 
    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/comment/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['author']").isString())
        .andExpect(jsonPath("$['commentaire']").isString())
        .andExpect(jsonPath("$['date_commentaire']").isNotEmpty())
        .andExpect(jsonPath("$['id_article']").isNumber());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/comment/1000"))
        .andExpect(status().isNotFound());
    }

    @Test
    void testPostComment() throws Exception {
        mvc.perform(
            post("/api/comment")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "author": "Mike",
                    "commentaire": " il est tres bon",
                    "date_commentaire": "2024-03-27",
                    "id_article": 1
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    //erreur expected bad request  400 but 201 created
    @Test
    void testPostInvalid() throws Exception {
        mvc.perform(
            post("/api/comment")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {

                    "commentaire": " il est tres bon",
                    "date_commentaire": "2024-03-27",
                    "id_article": 1
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteComment() throws Exception {
        mvc.perform(delete("/api/comment/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutComment() throws Exception {
        mvc.perform(put("/api/comment/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "author": "Mike",
                "commentaire": "il est tres bon",
                "date_commentaire": "2024-03-27",
                "id_article": 1
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['author']").value("Mike"))
        .andExpect(jsonPath("$['commentaire']").value("il est tres bon"))
        .andExpect(jsonPath("$['date_commentaire']").value("2024-03-27"))
        .andExpect(jsonPath("$['id_article']").value(1));
    }

}
