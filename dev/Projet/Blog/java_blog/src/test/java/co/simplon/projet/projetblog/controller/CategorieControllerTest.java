package co.simplon.projet.projetblog.controller;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql("/database.sql")

public class CategorieControllerTest {
    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception{
        mvc.perform(get("/api/categorie"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['name']").exists())
        .andExpect(jsonPath("$[0]['name']").value("plat"));
    }

    @Test
    void testGetOneSuccess() throws Exception{
        mvc.perform(get("/api/categorie/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['name']").isString());
    }

    @Test
    void testGetOneNotFound() throws Exception{
        mvc.perform(get("/api/categorie/10000"))
        .andExpect(status().isNotFound());
    }

    @Test
    // ??.andExpect(jsonPath("$['id']").isNumber());
    void testPostCategorie() throws Exception{
        mvc.perform(
            post("/api/categorie")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                    {
                        "name":"new"
                    }
                    """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testPostInvalidCategorie() throws Exception {
        mvc.perform(
            post("/api/categorie")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                    {
                        "name":
                    }
                    """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteCategorie() throws Exception{
        mvc.perform(
            delete("/api/categorie/2"))
            .andExpect(status().isNoContent());
    }

    @Test
    void testPutCategorie() throws Exception{
        mvc.perform(put("/api/categorie/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "name": "update"
            }
            """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['name']").value("update"));
    }

    }


