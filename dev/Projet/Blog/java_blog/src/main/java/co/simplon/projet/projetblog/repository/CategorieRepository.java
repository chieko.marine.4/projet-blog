package co.simplon.projet.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projet.projetblog.entity.Categorie;

@Repository
public class CategorieRepository {

    @Autowired
    private DataSource dataSource;
    
    public List<Categorie> findAll(){
        List<Categorie> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            {

                PreparedStatement stmt = connection.prepareStatement("SELECT * from categorie");
                ResultSet result = stmt.executeQuery();

                while(result.next()){
                    Categorie categorie = new Categorie(
                    result.getInt("id"),
                    result.getString("name")
                    );
                    list.add(categorie);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
}

public Categorie findById(int id){

    try (Connection connection = dataSource.getConnection()) {

            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categorie WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if(result.next()){
                Categorie categorie = new Categorie(
                result.getInt("id"),
                result.getString("name")
                );

            return categorie;
            }
        
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Error in repository", e);
    }
    return null;
}

public boolean persist(Categorie categorie) {


    try (Connection connection = dataSource.getConnection()) {

        PreparedStatement stmt = connection.prepareStatement("INSERT INTO categorie(name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, categorie.getName());

        if(stmt.executeUpdate() == 1){
            ResultSet keys = stmt.getGeneratedKeys();
            keys.next();
            categorie.setId(keys.getInt(1));
            return true;      
        }
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Error in repository", e);
    }
return false;
}

public boolean update(Categorie categorie) {


    try (Connection connection = dataSource.getConnection()) {

        PreparedStatement stmt = connection.prepareStatement("UPDATE categorie SET name=? WHERE id=?");
        stmt.setString(1, categorie.getName());
        stmt.setInt(2, categorie.getId());

        if(stmt.executeUpdate()==1){
            return true;      
        }
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Error in repository", e);
    }
return false;
}

public boolean delete(int id) {

    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM categorie WHERE id= ?");
        stmt.setInt(1, id);

        if(stmt.executeUpdate() == 1){
            return true;      
        }
    } catch (SQLException e) {
        e.printStackTrace();
        throw new RuntimeException("Error in repository", e);
    }
    return false;
}


}
