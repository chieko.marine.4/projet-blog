package co.simplon.projet.projetblog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projet.projetblog.entity.User;


@Repository
public class UserRepository {
    @Autowired
    private DataSource dataSource;

    public Optional<User> findByEmail(String email) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                User user = sqlToUser(rs);
                return Optional.of(user);
            }


        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }

        return Optional.empty();
    }

    public boolean persist(User user) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO user (email,password,role) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getRole());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                if(rs.next()) {
                    user.setId(rs.getInt(1));
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }

        return false;
    }

    private User sqlToUser(ResultSet rs) throws SQLException {
        return new User(
            rs.getInt("id"), 
            rs.getString("email"), 
            rs.getString("password"), 
            rs.getString("role"));
    }
}

