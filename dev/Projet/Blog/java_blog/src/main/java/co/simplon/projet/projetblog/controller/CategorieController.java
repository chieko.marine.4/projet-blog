package co.simplon.projet.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.projet.projetblog.entity.Categorie;
import co.simplon.projet.projetblog.repository.CategorieRepository;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.PutMapping;



@RestController
@CrossOrigin("*")
@RequestMapping("/api/categorie")

public class CategorieController {

    @Autowired
    private CategorieRepository categoRepo;

    @GetMapping
    public List<Categorie> all() {
        return categoRepo.findAll();
    }

    @GetMapping("/{id}")
    public Categorie one(@PathVariable int id){

        Categorie categorie = categoRepo.findById(id);

        if(categorie == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return categorie;
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categorie categorie(@Valid @RequestBody Categorie categorie){
        categoRepo.persist(categorie);
        return categorie;
    }

    //?one(id)?
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id){
        one(id);
        categoRepo.delete(id);
    }
    
    @PutMapping("/{id}")
    public Categorie replace(@PathVariable int id, @Valid@RequestBody Categorie categorie) {
        one(id);
        categorie.setId(id);
        categoRepo.update(categorie);
        return categorie;
    }

}
