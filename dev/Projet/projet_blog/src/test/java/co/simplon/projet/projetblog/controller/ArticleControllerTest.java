package co.simplon.projet.projetblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;


@SpringBootTest 
@AutoConfigureMockMvc 
@Sql("/database.sql")
public class ArticleControllerTest {

@Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/article"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['titre']").exists())
        .andExpect(jsonPath("$[0]['titre']").value("Nom de plat"));
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/article/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['titre']").isString())
        .andExpect(jsonPath("$['photo']").isString())
        .andExpect(jsonPath("$['text']").isString());
    }

    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/article/1000"))
        .andExpect(status().isNotFound());
    }

    @Test
    // ??.andExpect(jsonPath("$['id']").isNumber());
    void testPostArticle() throws Exception {
        mvc.perform(
            post("/api/article")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "titre": "new",
                    "photo": "new",
                    "text": "new",
                    "date_article": "2024-01-01" ,
                    "vue": 15,
                    "id_categorie": 3
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }

    //??badRequest
    @Test
    void testPostInvalidArticle() throws Exception {
        mvc.perform(
            post("/api/article")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "titre": "create",
                    "photo": "create",
                    "text": "create",
                    "date_article": "2024-01-01" ,
                    "vue": 1
                    "id_categorie": 3
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    //expected 204 but 404
    @Test
    void testDeleteArticle() throws Exception {
        mvc.perform(delete("/api/article/3"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutArticle() throws Exception {
        mvc.perform(put("/api/article/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "titre": "update",
                "photo": "update",
                "text": "update",
                "date_article": "2024-01-01" ,
                "vue": 15,
                "id_categorie": 3
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['titre']").value("update"))
        .andExpect(jsonPath("$['photo']").value("update"))
        .andExpect(jsonPath("$['text']").value("update"))
        .andExpect(jsonPath("$['date_article']").value("2024-01-01"))
        .andExpect(jsonPath("$['vue']").value(15))
        .andExpect(jsonPath("$['id_categorie']").value(3));
    }
    





}
