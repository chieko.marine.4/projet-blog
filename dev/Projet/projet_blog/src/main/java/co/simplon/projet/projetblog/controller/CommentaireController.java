package co.simplon.projet.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.projet.projetblog.entity.Commentaire;
import co.simplon.projet.projetblog.repository.CommentaireRepository;
import jakarta.validation.Valid;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/comment")

public class CommentaireController {
    @Autowired
    private CommentaireRepository commentRepo;

    @GetMapping
    public List<Commentaire> all() {
        return commentRepo.findAll();
    }

    @GetMapping("/{id}")
    public Commentaire one(@PathVariable int id) {
        Commentaire commentaire = commentRepo.findById(id);
        if(commentaire == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return commentaire;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Commentaire commentaire( @Valid @RequestBody Commentaire commentaire) {
        commentRepo.persist(commentaire);
        return commentaire;
    }

    
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        commentRepo.delete(id);
    }

    
    //ok thunder  sans id sur JSON
    @PutMapping("/{id}")
    public Commentaire replace(@PathVariable int id, @Valid @RequestBody Commentaire commentaire) {
        one(id); 
        commentaire.setId(id);
        commentRepo.update(commentaire);
        return commentaire;
    }


}
