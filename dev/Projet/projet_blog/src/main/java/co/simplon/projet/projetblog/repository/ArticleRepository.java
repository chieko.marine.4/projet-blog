package co.simplon.projet.projetblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projet.projetblog.entity.Article;

@Repository
public class ArticleRepository {

    @Autowired
    private DataSource dataSource;


    public List<Article> findAll(){
        List<Article> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Article article = new Article(
                        result.getInt("id"),
                        result.getString("titre"),
                        result.getString("photo"),
                        result.getString("text"),
                        result.getDate("date_article").toLocalDate(),
                        result.getInt("id_categorie"),
                        result.getInt("vue")
                        );
                list.add(article);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Article article = new Article(
                    result.getInt("id"),
                        result.getString("titre"),
                        result.getString("photo"),
                        result.getString("text"),
                        result.getDate("date_article").toLocalDate(),
                        result.getInt("id_categorie"),
                        result.getInt("vue")
                    );
                return article;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public boolean persist(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO article (titre,photo,text,date_article,vue,id_categorie) VALUES (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, article.getTitre());
            stmt.setString(2, article.getPhoto());
            stmt.setString(3, article.getText());
            stmt.setDate(4, Date.valueOf(article.getDate_article()));
            stmt.setInt(5, article.getVue());
            stmt.setInt(6, article.getId_categorie());

            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                article.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }


    public boolean update(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET titre=?,photo=?,text=?,date_article=?,vue=?,id_categorie=? WHERE id=?");
            stmt.setString(1, article.getTitre());
            stmt.setString(2, article.getPhoto());
            stmt.setString(3, article.getText());
            stmt.setDate(4, Date.valueOf(article.getDate_article()));
            stmt.setInt(5, article.getVue());
            stmt.setInt(6, article.getId_categorie());
            stmt.setInt(7, article.getId());

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }


    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
            stmt.setInt(1, id);

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    public List<Article> findByIdCategorie(int id){
        List<Article> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id_categorie=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Article article = new Article(
                        result.getInt("id"),
                        result.getString("titre"),
                        result.getString("photo"),
                        result.getString("text"),
                        result.getDate("date_article").toLocalDate(),
                        result.getInt("id_categorie"),
                        result.getInt("vue")
                        );
                list.add(article);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    //essaie
    public Article findByIdCatego(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id_categorie=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                Article article = new Article(
                    result.getInt("id"),
                        result.getString("titre"),
                        result.getString("photo"),
                        result.getString("text"),
                        result.getDate("date_article").toLocalDate(),
                        result.getInt("id_categorie"),
                        result.getInt("vue")
                    );
                return article;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }


// ////

//     public Article findByMot(String mot) {
//         try (Connection connection = dataSource.getConnection()) {
//             PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE text LIKE'%?%'");
//             stmt.setString(1, mot);
//             ResultSet result = stmt.executeQuery();
//             if (result.next()) {
//                 Article article = new Article(
//                     result.getInt("id"),
//                     result.getString("titre"),
//                     result.getString("photo"),
//                     result.getString("text"),
//                     result.getDate("date_article").toLocalDate(),
//                     result.getInt("id_categorie"),
//                     result.getInt("vue")
//                     );
//                 return article;
//             }

//         } catch (SQLException e) {
//             e.printStackTrace();
//             throw new RuntimeException("Error in repository", e);
//         }
//         return null;
//     }
}
