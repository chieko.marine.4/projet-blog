package co.simplon.projet.projetblog;


    

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class CorsConfiguration implements WebMvcConfigurer{
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/api/**")
// Mettez le bon port de votre projet vuejs 5174 ou autre
        .allowedOrigins("http://localhost:5173")
        .allowedMethods("*");
    }
}

