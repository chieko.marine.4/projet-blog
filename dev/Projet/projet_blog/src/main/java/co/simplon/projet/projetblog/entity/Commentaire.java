package co.simplon.projet.projetblog.entity;

import java.time.LocalDate;

public class Commentaire {
    private int id;
    private String author;
    private String commentaire;
    private LocalDate date_commentaire;
    private int id_article;
    
    public Commentaire() {
    }
    public Commentaire(String author, String commentaire, LocalDate date_commentaire, int id_article) {
        this.author = author;
        this.commentaire = commentaire;
        this.date_commentaire = date_commentaire;
        this.id_article = id_article;
    }
    public Commentaire(int id, String author, String commentaire, LocalDate date_commentaire, int id_article) {
        this.id = id;
        this.author = author;
        this.commentaire = commentaire;
        this.date_commentaire = date_commentaire;
        this.id_article = id_article;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getCommentaire() {
        return commentaire;
    }
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    public LocalDate getDate_commentaire() {
        return date_commentaire;
    }
    public void setDate_commentaire(LocalDate date_commentaire) {
        this.date_commentaire = date_commentaire;
    }
    public int getId_article() {
        return id_article;
    }
    public void setId_article(int id_article) {
        this.id_article = id_article;
    }
    
    

}
