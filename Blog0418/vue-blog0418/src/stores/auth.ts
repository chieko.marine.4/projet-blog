import { fetchLogin, fetchLogout , postRegister} from "@/auth-service";
import type { User } from "@/entities";
import { defineStore } from "pinia";
import { ref } from "vue";


export const useAuth = defineStore('auth', () => {

    const user = ref<User>();
    const stored = localStorage.getItem('user');
    if(stored) {
        user.value = JSON.parse(stored);
    }

    async function login(email:string,password:string) {
        const data = await fetchLogin(email,password);
        localStorage.setItem('user', JSON.stringify(data));
        user.value = data;
    }

    async function logout() {
        await fetchLogout();
        localStorage.removeItem('user');
        user.value = undefined;
    }

    async function signup(userNew: User) {
        try {
            const data = await postRegister(userNew);
            // Consider setting user data or handling post-signup actions here
            localStorage.setItem('user', JSON.stringify(data));
            user.value = data;
        } catch (error) {
            console.error("Signup failed:", error);
            throw error;
        }
    }

    return {user,login,logout,signup};
})
