import axios from "axios";
import type { User } from "./entities";


export async function fetchLogin(email:string, password:string) {
    const response = await axios.get<User>('/api/account', {
        auth: {
            username: email,
            password
        }
    });
    return response.data
}

export async function fetchLogout() {
    await axios.get<void>('/logout');
}

export async function postRegister(user:User) {
    const response = await axios.post<User>('/api/user', user);
    return response.data
}
