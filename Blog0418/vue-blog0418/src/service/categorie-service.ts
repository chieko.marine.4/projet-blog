import type { Categorie } from "@/entities";
import axios from "axios";

export async function fetchCategories() {
    const reponse = await axios.get<Categorie[]>('http://localhost:8080/api/categorie')
    return reponse.data
}

export async function fetchIdCategories(id:any) {
    const reponse = await axios.get<Categorie[]>('http://localhost:8080/api/categorie/' +id)
    return reponse.data
}

export async function deleteCategories(id:any) {
    await axios.delete<void>('http://localhost:8080/api/categorie/' +id)
}

export async function postCategories(categorie:Categorie) {
    const reponse = await axios.post<Categorie>('http://localhost:8080/api/categorie',categorie)
    return reponse.data
}

export async function updateCategories(categorie:Categorie) {
    const reponse = await axios.put<Categorie>('http://localhost:8080/api/categorie/' + categorie.id, categorie)
    return reponse.data
}