import type { Commentaire } from "@/entities";
import axios from "axios";

export async function fetchCommentaires() {
    const reponse = await axios.get<Commentaire[]>('http://localhost:8080/api/categorie')
    return reponse.data
}