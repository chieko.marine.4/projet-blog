import type { Categorie } from "@/entities";
import axios from "axios";

export async function fetchCategories() {
    const reponse = await axios.get<Categorie[]>('http://localhost:8080/api/categorie')
    return reponse.data
}

export async function fetchIdCategories(id:any) {
    const reponse = await axios.get<Categorie[]>('http://localhost:8080/api/categorie/' +id)
    return reponse.data
}

export async function fetchDeleteCategories() {
    const reponse = await axios.delete<Categorie[]>('http://localhost:8080/api/categorie/' +id)
    return reponse.data
}

export async function fetchCreateCategories() {
    const reponse = await axios.post<Categorie[]>('http://localhost:8080/api/ategorie/')
    return reponse.data
}

export async function fetchUpdateCategories() {
    const reponse = await axios.put<Categorie[]>('http://localhost:8080/api/categorie/' +id)
    return reponse.data
}