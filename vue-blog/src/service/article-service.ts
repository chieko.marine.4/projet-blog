import ArticleList from "@/components/ArticleList.vue";
import type { Article } from "@/entities";
import axios from "axios";

export async function fetchAllArticles() {
    const reponse = await axios.get<Article[]>('http://localhost:8080/api/article')
    return reponse.data
}

export async function fetchIdArticles(id:any) {
    const reponse1 = await axios.get<Article[]>('http://localhost:8080/api/article/' +id)
    return reponse1.data
}

export async function fetchIdCategoArticles(id_categorie:any) {
    const reponse1 = await axios.get<Article[]>('http://localhost:8080/api/article/catego' +id_categorie)
    return reponse1.data
}

// export async function fetchDeleteArticles() {
//     const reponse1 = await axios.delete<Article[]>('http://localhost:8080/api/article/' + article.id)
//     return reponse1.data
// }

// export async function fetchCreateArticles() {
//     const reponse1 = await axios.post<Article[]>('http://localhost:8080/api/article/')
//     return reponse1.data
// }

// export async function fetchUpdateArticles() {
//     const reponse1 = await axios.put<Article[]>('http://localhost:8080/api/article/' + article.id)
//     return reponse1.data
// }