import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import CategorieVue from '@/views/CategorieVue.vue'
import ArticleVue from '@/views/ArticleVue.vue'
import CommentaireVue from '@/views/CommentaireVue.vue'
import Plat_dessertList from '@/views/Plat _dessertList.vue'
import Plat_dessertVue from '@/views/Plat_dessertVue.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', component: HomeView},
    { path: '/categorie', component: CategorieVue },
    { path: '/article', component: ArticleVue },
    { path: '/commentaire', component: CommentaireVue },
    { path: '/plat_dessertList', component: Plat_dessertList },
    { path: '/plat_dessert', component: Plat_dessertVue }

  ]
})

export default router
