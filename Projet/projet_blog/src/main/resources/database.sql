-- Active: 1709545648155@@127.0.0.1@3306@projet_blog
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS categorie;
DROP TABLE IF EXISTS commentaire;

CREATE TABLE categorie (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (100) NOT NULL
);
CREATE TABLE article (
    id INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(100) NOT NULL,
    photo VARCHAR(250) NOT NULL,
    text VARCHAR(650) NOT NULL,
    date_article DATE,
    vue INT,
    id_categorie INT,
    Foreign Key (id_categorie) REFERENCES categorie(id)
);


CREATE TABLE commentaire (
    id INT PRIMARY KEY AUTO_INCREMENT,
    author VARCHAR(100),
    commentaire VARCHAR(350),
    date_commentaire DATE,
    id_article INT,
    Foreign Key (id_article) REFERENCES article(id)
);
INSERT INTO categorie(name) VALUES
('plat'),
('dessert'),
('produit'),
('magasin'),
('restaurant');

INSERT INTO article (titre,photo, text,date_article,vue, id_categorie) VALUES
('Nom de plat','url','recette okonomiyaki','2024-01-01',1,1),
('Nom de dessert','url','recette de Anko ','2024-01-02',1,2),
('Nom de adresse','url','Bon adresse de restaurant','2024-01-03',1,3),
('Nom de produit','url','Ce produit.......','2024-01-04',1,4);
 

INSERT INTO commentaire(author, commentaire, date_commentaire, id_article) VALUES
('Mike',' il est tres bon','2024-03-27',1),
('Tom',' il est parfait','2024-04-25',2),
('Leo',' Je vous recommande','2024-05-20',3),
('Anna',' cest un bon adresse','2024-06-15',4);


SELECT * FROM article; 


