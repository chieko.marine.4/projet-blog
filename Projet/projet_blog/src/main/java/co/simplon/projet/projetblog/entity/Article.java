package co.simplon.projet.projetblog.entity;

import java.time.LocalDate;


public class Article {

    private int id;
    private String titre;
    private String photo;
    private String text;
    private LocalDate date_article;
    private int id_categorie;
    private int vue;
    
    public Article() {
    }
    
    public Article(String titre, String photo, String text, LocalDate date_article, int id_categorie, int vue) {
        this.titre = titre;
        this.photo = photo;
        this.text = text;
        this.date_article = date_article;
        this.id_categorie = id_categorie;
        this.vue = vue;
    }
    public Article(int id, String titre, String photo, String text, LocalDate date_article, int id_categorie, int vue) {
        this.id = id;
        this.titre = titre;
        this.photo = photo;
        this.text = text;
        this.date_article = date_article;
        this.id_categorie = id_categorie;
        this.vue = vue;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }
    public String getPhoto() {
        return photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public LocalDate getDate_article() {
        return date_article;
    }
    public void setDate_article(LocalDate date_article) {
        this.date_article = date_article;
    }
    public int getId_categorie() {
        return id_categorie;
    }
    public void setId_categorie(int id_categorie) {
        this.id_categorie = id_categorie;
    }
    public int getVue() {
        return vue;
    }
    public void setVue(int vue) {
        this.vue = vue;
    }

    
    
    


}
