package co.simplon.projet.projetblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.projet.projetblog.entity.Article;
import co.simplon.projet.projetblog.repository.ArticleRepository;
import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;


@RestController
@RequestMapping("/api/article")
public class ArticleController {@gitlab.com:chieko.marine.4/projet-blog.git

    @Autowired
    private ArticleRepository artiRepo;

    @GetMapping
    public List<Article> all() {
        return artiRepo.findAll();
    }

    @GetMapping("/{id}")
    public Article one(@PathVariable int id) {
        Article article = artiRepo.findById(id);
        if(article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return article;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article add( @Valid @RequestBody Article article) {
        artiRepo.persist(article);
        return article;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        artiRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Article replace(@PathVariable int id, @Valid @RequestBody Article article) {
        one(id); 
        article.setId(id);
        artiRepo.update(article);
        return article;
    }



    

}
