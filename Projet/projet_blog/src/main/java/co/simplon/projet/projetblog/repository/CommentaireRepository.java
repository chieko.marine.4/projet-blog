package co.simplon.projet.projetblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.projet.projetblog.entity.Article;
import co.simplon.projet.projetblog.entity.Commentaire;

@Repository
public class CommentaireRepository {

    @Autowired
    private DataSource dataSource;


    public List<Commentaire> findAll(){
        List<Commentaire> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commentaire");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Commentaire commentaire = new Commentaire(
                        result.getInt("id"),
                        result.getString("author"),
                        result.getString("commentaire"),
                        result.getDate("date_commentaire").toLocalDate()
                        );
                list.add(commentaire);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commentaire WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Commentaire commentaire = new Commentaire(
                        result.getInt("id"),
                        result.getString("author"),
                        result.getString("commentaire"),
                        result.getDate("date_commentaire").toLocalDate()
                        );
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public boolean persist(Commentaire commentaire) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO commentaire (author,commentaire,date_categorie) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, commentaire.getAuthor());
            stmt.setString(2, commentaire.getCommentaire());
            stmt.setDate(3, Date.valueOf(commentaire.getDate_commentaire()));


            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                commentaire.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

/* */    public boolean update(Commentaire commentaire) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET author=?,commentare=?,date_categorie=?,id_categorie=? WHERE id=?;");
            stmt.setString(1, commentaire.getAuthor());
            stmt.setString(2, commentaire.getCommentaire());
            stmt.setDate(3, Date.valueOf(commentaire.getDate_commentaire()));

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");

            
            stmt.setInt(1, id);

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

}
